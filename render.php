<?php
error_reporting(E_ALL);
require_once 'include/db/crud.php';

function insert_marca($marca,$marca_m1)
{
	global $total;
	$retorno = NULL;

	if($marca != $marca_m1) {
		$retorno ="
		<table></table>
		<table>
		  <tr>
		    <th style='text-align: left; min-width: 188.5mm; max-width: 188.5mm; background-color: #28367b; font-size: 20px; height: 7mm; color:#fff'>
		      <strong>".$marca."</strong>
		    </th>
		  </tr>
		</table>
		<table></table>
		";
	}

  return $retorno;
}

function insert_nome_veiculo($nome_veiculo,$nome_veiculo_m1)
{
  $retorno = NULL;
  global $total;

	if($nome_veiculo != $nome_veiculo_m1) {
	$retorno ="
		<table>
		  <tr>
		  <th style='text-align: left; min-width: 188.5mm; max-width: 188.5mm; background-color: #d9d6e5; height: 4.5mm; font-size: 10pt; max-width: 187.6mm;'>
		      <strong>".$nome_veiculo."</strong>
		  </th>
		</tr>
		</table>
		<table>
		";
	}

	return $retorno;
}

$pagina = "
<!DOCTYPE HTML>
<html charset='UTF-8'>
<head>
<title>Catalogo Integra AG</title>
<meta charset='utf-8'>
<link rel='stylesheet' href='include/style/pag_cat_principal.css' type='text/css' />
<style>
	td {
		padding: 0;
	}
	* {
		border-collapse: collapse;
	}
</style>
</head>
<body style='max-width: 210mm; min-width: 210mm; border: 1px solid black; height: 1300px;'>
	
	<table style='min-width: 210mm; max-width: 210mm; min-height: 8.250mm; background-color: #28367b; margin-bottom: 10mm;'>
		<tr>
			<th style='min-height: 18mm; max-height: 18mm;'>&nbsp;</th>
		</tr>
	</table>
	
	<div style='margin-left: 12.5mm'>
	<!-- CABEÇALHO -->
	<div style='width: 185mm; height: 1px; border-bottom: 1px solid black; margin-top: -1px; position: absolute;'></div>
	<table style='min-height: 13.424mm; max-height: 13.424mm; margin-bottom: 1.60mm;'>
		<tr style='max-width: 185.176mm; min-width: 185.176mm; max-height: 13.424mm; min-height: 13.424mm;'>
			<th style='min-width: 30.088mm; max-width: 30.088mm; min-height: 13.424mm; max-height: 13.424mm;' ><img style='width:22.969mm; height: 9.600mm;' src='./imgs/icone1.png'></th>
			<th style='min-width: 33mm; max-width: 33mm; min-height: 13.424mm; max-height: 13.424mm; border-right-color: #fff; border-left: #fff;'><img style='width:11.602mm; height: 9.600mm;' src='./imgs/icone2.png'></th>
			<th style='max-width: 0.5mm; min-width: 0.5mm; min-height: 13.424mm; max-height: 13.424mm;'></th>
			<th style='min-width: 14.800mm; max-width: 14.800mm; min-height: 13.424mm; max-height: 13.424mm; border-left-color: #fff;'><img style='width:8.083mm; height: 9.600mm;' src='./imgs/icone3.png'></th>
			<th style='min-width: 12mm; max-width: 12mm; min-height: 13.424mm; max-height: 13.424mm;'><img style='width:9.300mm;' src='./imgs/icone4.png'></th>
			<th style='min-width: 29mm; max-width: 29mm; font-size: 9pt; min-height: 13.424mm; max-height: 13.424mm;'>Descrição</th>
			<th style='min-width: 33.600mm; max-width: 33.600mm; font-size: 9pt; min-height: 13.424mm; max-height: 13.424mm;'>Posição</th>
			<th style='min-width: 9mm; max-width: 9mm; font-size: 9pt; min-height: 13.424mm; max-height: 13.424mm;'>Quant.</th>
			<th style='min-width: 23.8mm; max-width: 23.8mm; min-height: 13.424mm; max-height: 13.424mm;'><img style='width:9.600mm; height: 9.600mm;' src='./imgs/icone8.png'></th>
		</tr>
	</table>
	<div style='width: 185mm; height: 1px; border-bottom: 1px solid black; margin-top: -8px; position: absolute;''></div>
	<table></table>

	";

	// var_dump($result); //DEBUG é só para ver o array que vem do PDO

	foreach ($result as $key => $value) {

		// $pagina .= "
		// 		<table>
		// 		  	<tr>
		// 		    	<th style='text-align: left; min-width: 189mm; max-width: 189mm; background-color: #28367b; font-size: 20px; height: 7mm; color:#fff'>
		// 		      		<strong>".$result[$key]["marca"]."</strong>
		// 		    	</th>
		// 		  	</tr>
		// 	  	</table>";
		// $pagina .= "Marca $key: ". $result[$key]["marca"];
		// $pagina .= "Marca $key-1: ". $result[$key-1]["marca"];

		$pagina .= insert_marca($result[$key]["marca"],$result[$key-1]["marca"]);
		$pagina .= insert_nome_veiculo($result[$key]["nome_veiculo"],$result[$key-1]["nome_veiculo"]);

		if($result[$key]["modelo_veiculo"] == '') {
			$modelo_veiculo = "<td style='min-width: 30.088mm; max-width: 30.088mm;'>".$result[$key]["modelo_veiculo"]."</td>";
		} else {
			$modelo_veiculo = "<td style='min-width: 30.088mm; max-width: 30.088mm;'>".$result[$key]["modelo_veiculo"]."</td>";
		}

		$pagina .= "
				<tr>
					".$modelo_veiculo."
					<td style='min-width: 10mm; max-width: 10mm;'>".$result[$key]["motor_cil"]."</td>
					<td style='min-width: 10mm; max-width: 10mm;'>".$result[$key]["pot_cv"]."</td>
					<td style='min-width: 13mm; max-width: 13mm;'>".$result[$key]["nome_motor"]."</td>
					<td style='min-width: 14.800mm; max-width: 14.800mm;'>".$result[$key]["data"]."</td>
					<td style='min-width: 12mm; max-width: 12mm; background-color: #d9d6e5; text-align: center;'><strong>".$result[$key]["rev_ruville"]."</strong></td>
					<td style='min-width: 29mm; max-width: 29mm;'>".$result[$key]["nome_brasil"]."</td>
					<td style='min-width: 33.600mm; max-width: 33.600mm;'>".$result[$key]["posicao"]."</td>
					<td style='min-width: 9mm; max-width: 9mm; text-align: center;'>".$result[$key]["qtd"]."</td>
					<td style='min-width: 23.688mm; max-width: 23.688mm;'>".$result[$key]["obs"]."</td>
				</tr>";
	}


	$pagina .=	"
		</table>
	</body>
	</html>
	";